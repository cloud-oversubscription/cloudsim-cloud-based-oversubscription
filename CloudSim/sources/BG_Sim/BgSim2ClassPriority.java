
package BG_Sim;

/*
 * Title:        CloudSim Toolkit
 * Description:  CloudSim (Cloud Simulation) Toolkit for Modeling and Simulation
 *               of Clouds
 * Licence:      GPL - http://www.gnu.org/copyleft/gpl.html
 *
 * Copyright (c) 2009, The University of Melbourne, Australia
 * 
 * 
 * This code is based on CloudSim Example 3 and on 
 * code used in the paper:
 * 
 *  We added code to track the CPU time and calculate VM debt
 */

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;

import org.cloudbus.cloudsim.Cloudlet;
import org.cloudbus.cloudsim.CloudletSchedulerSpaceShared;
import org.cloudbus.cloudsim.CloudletSchedulerTimeShared;
import org.cloudbus.cloudsim.Datacenter;
import org.cloudbus.cloudsim.DatacenterBroker;
import org.cloudbus.cloudsim.DatacenterCharacteristics;
import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.Pe;
import org.cloudbus.cloudsim.Storage;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.VmAllocationPolicySimple;
import org.cloudbus.cloudsim.VmScheduler;
import org.cloudbus.cloudsim.VmSchedulerTimeShared;
import org.cloudbus.cloudsim.VmSchedulerTimeSharedOverSubscription;
import org.cloudbus.cloudsim.core.CloudSim;
import org.cloudbus.cloudsim.provisioners.BwProvisionerSimple;
import org.cloudbus.cloudsim.provisioners.PeProvisionerSimple;
import org.cloudbus.cloudsim.provisioners.RamProvisionerSimple;
import org.cloudbus.cloudsim.util.WorkloadFileReader;

/**
 * BgSim2ClassPriority
 * This class contains the main method that serves as the driver class for Experiments 1 and 2.
 *
 */

public class BgSim2ClassPriority {

	static FileOutputStream out;
	static PrintStream ps;
	
	static ArrayList<Vm> vmlist;

	/**
	 * Creates main() to run this example.
	 * 
	 * @param args
	 *            the args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		
		BufferedReader in = new BufferedReader(new FileReader("Simulation Files/input/Adjusted5000Vms.txt")); 
		
		while (in.ready()) {
			String input = in.readLine();
			StringTokenizer tokenizer = new StringTokenizer(input,"|");
			String workload = tokenizer.nextToken();
			String simName = tokenizer.nextToken();
			String simType = tokenizer.nextToken();
			int numVMs = Integer.parseInt(tokenizer.nextToken()); 
			int numPriorityVMs = Integer.parseInt(tokenizer.nextToken());
			int priority = Integer.parseInt(tokenizer.nextToken());
			int nonPriorityMIPS = Integer.parseInt(tokenizer.nextToken());
			int expNum = Integer.parseInt(tokenizer.nextToken());
			int allocationType = Integer.parseInt(tokenizer.nextToken());
			
	
		
			try {
				out = new FileOutputStream("Simulation Files/output/" + expNum + "_" + simName);
			} 
			catch (FileNotFoundException e) { 
				e.printStackTrace();
			}
		
			ps = new PrintStream(out);
		
			vmlist = new ArrayList<Vm>();
			initSimulation(workload, simName, simType, numVMs, numPriorityVMs, priority, nonPriorityMIPS, expNum, allocationType);
		
			ps.close();
		}
		
		in.close();
		
	}

	private static void initSimulation(String workload, String simName, String simType, int numVMs, int numPriorityVMs, int priority, int nonPriorityMIPS, int expNum, int allocationType) {
		Log.printLine("Starting CloudSim simulation Example...");

		try {

			// [1]: Initialize the CloudSim package. 
			// It should be called before creating any entities. 
			int num_user = 1; 								// number of cloud users
			Calendar calendar = Calendar.getInstance(); 	// get calendar using
															// current time zone
			boolean trace_flag = false; 					// mean trace events
			CloudSim.init(num_user, calendar, trace_flag); 	// Initialize the
														   	// CloudSim library

			// [2]: Create Datacenter
			@SuppressWarnings("unused")
			Datacenter datacenter1 = createDatacenter("Datacenter_1", allocationType);

			// [3]: Create Cloud Broker and name it Broker1
			DatacenterBroker broker = createBroker(1);
			int brokerId = broker.getId(); 	

			// [4]: Create virtual machines that use 
			// time-shared cloudlet scheduling
			// Use the 4th parameter to vary mips for priority
			// Use the 5th parameter to add priority (1=Priority VM)
			//Use the 6th parameter to mark if it was a priority VM at any time for comparison
			addVMs(numVMs - numPriorityVMs, brokerId, true, nonPriorityMIPS, 0, 0);
			addVMs(numPriorityVMs, brokerId, true, 6000, priority, 1);
						
			// [5]: submit vm list to the broker
			broker.submitVmList(vmlist);

			
			// [6]: Read the workload file and create Cloudlets from it
			List<Cloudlet> cloudletList = createCloudLets(workload);			
			
			// [7]: Set all cloudlets to be managed by one broker.
			for (Cloudlet cloudlet : cloudletList) {
				cloudlet.setUserId(brokerId);
			}

			// [8]: Submit cloudlet list to the broker
			broker.submitCloudletList(cloudletList);

			// [9]: Start the simulation
			CloudSim.startSimulation();

			// [10]: Stop the simulation
			CloudSim.stopSimulation();


			// retrieve all recieved cloudlet list
			List<Cloudlet> newCList = broker.getCloudletReceivedList();
			
			// retrieve list of all VMs
			List<Vm> newVList = broker.getVmList();
			

			// [11]: print the simulation results
			printResults(newCList, newVList, simName, workload, simType, expNum, numPriorityVMs);

			ps.println("CloudSimExample finished!");

		} catch (Exception e) {
			e.printStackTrace();
			Log.printLine("Unwanted errors happen");
		}
	}

	/**
	 * Creates the datacenter.
	 * 
	 * desc: Datacenters are the resource providers in CloudSim. We need at
	 * least one of them to run a CloudSim simulation
	 * 
	 * @param name
	 *            the name of the data center
	 * 
	 * @return the datacenter
	 */
	private static Datacenter createDatacenter(String name, int allocationType) {

		List<Host> hostList = new ArrayList<Host>();

		//create 50 hosts with 8 cores each so we have 400 cores
		for(int i = 0 ; i < 500; i++){
			
			//create a list of Pes (Physical elements)
			List<Pe> peList = new ArrayList<Pe>();
			
			//Host Properties
			int mips = 48000; 	//total mips in one host
			int cores = 8;
			int id = i+1;
			//int ram = 16384; 	// host memory (MB)
			//int ram = 18432;
			int ram = 20480;
			long storage = 1048576; 
			int bw = 10240; 	// bandwidth in MB/s

			//Provision the number of MIPS per PE, 6,000 in this case
			for (int j = 0; j < cores; j++) {
				peList.add(new Pe(j, new PeProvisionerSimple(mips/ cores)));
			}

			//for chooseVMScheduler true = time shared oversubscription ... false = time shared
			hostList.add(new Host(id, new RamProvisionerSimple(ram), new BwProvisionerSimple(bw), storage, 
					peList, chooseVMScheduler(allocationType, peList)));
		}
		
		// Create a DatacenterCharacteristics object that stores the
		// properties of a data center: architecture, OS, list of
		// machines, allocation policy: time- or space-shared, time zone
		// and its price (G$/Pe time unit).
		String arch = "x86"; 			// system architecture
		String os = "Linux"; 			// operating system
		String vmm = "Xen";
		double time_zone = 10.0; 		// time zone this resource located
		double cost = 3.0; 				// the cost of using processing in this resource
		double costPerMem = 0.05; 		// the cost of using memory in this resource
		double costPerStorage = 0.001; 	// the cost of using storage in this
										// resource
		double costPerBw = 0.0; 		// the cost of using bw in this resource
		
		// we are not adding SAN devices by now
		LinkedList<Storage> storageList = new LinkedList<Storage>();

		DatacenterCharacteristics characteristics = new DatacenterCharacteristics(
				arch, os, vmm, hostList, time_zone, cost, costPerMem,
				costPerStorage, costPerBw);

		// Finally, we need to create a PowerDatacenter object.
		Datacenter datacenter = null;
		try {
			datacenter = new Datacenter(name, characteristics,
					new VmAllocationPolicySimple(hostList), storageList, 0);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return datacenter;
	}
	
	// Used to determine if we are using oversubscription or not
	public static VmScheduler chooseVMScheduler(int x, List<Pe> peList) {
		if(x == 1){
			return new VmSchedulerTimeSharedOverSubscription(peList);
		}
		else if(x == 2){
			return new VmSchedulerTimeSharedOversubscriptionPriority(peList);
		}
		else{
			return new VmSchedulerTimeShared(peList);
		}	
		
	}
	
	
	/**
	 * Creates the broker.
	 * 
	 * @param id
	 *            : the broker id
	 * 
	 * @return the datacenter broker
	 */
	private static DatacenterBroker createBroker(int id) {
		DatacenterBroker broker = null;
		try {
			broker = new DatacenterBroker("Broker" + id);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return broker;
	}

	/**
	 * Creates the virtual machines.
	 * 
	 * @param VMNr
	 *            : the number of virtual machines to create brokerId: the id of
	 *            the broker created timeSharedScheduling: to choose between the
	 *            time shared or space shared scheduling algorithms
	 * 
	 * @return list of virtual machines
	 * 
	 */
	private static void addVMs(int VMNr, int brokerId, boolean timeSharedScheduling, 
								double mips, int priority_level, int priority_marker) {

		// VM properties
		long size = 10240; // image size (MB)
		int ram = 2048; 	// vm memory (MB)
		long bw = 100; 		// MB/s
		int pesNumber = 2; 	// number of cpus
		String vmm = "Xen"; // VMM name
		
		
		for (int i = 0; i < VMNr; i++) {

			Vm vm;
			
			int VM_ID = vmlist.size();
			
			if (timeSharedScheduling) {
				// create VM that uses time shared scheduling to schedule cloudlets
				vm = new Vm(VM_ID, brokerId, mips, priority_level, priority_marker, 
						pesNumber, ram, bw, size, vmm, new CloudletSchedulerTimeShared());
			}

			else {
				// create VM that uses space shared scheduling to schedule cloudlets
				vm = new Vm(VM_ID, brokerId, mips, priority_level, priority_marker, 
						pesNumber, ram, bw, size, vmm, new CloudletSchedulerSpaceShared());
			}

			// Add the VM to the vmList
			vmlist.add(vm);
		}
	}

	/**
	 * Generate cloudlets from the workload file
	 * 
	 * @return list of cloudlets
	 * 
	 */
	private static List<Cloudlet> createCloudLets(String workload)
			throws FileNotFoundException {

		/** The cloudlet list. */
		List<Cloudlet> cloudletList;

		// Read Cloudlets from workload file in the swf format
		WorkloadFileReader workloadFileReader = new WorkloadFileReader(
				workload, 1);
				//"Simulation Files/NASA-iPSC-1993-3.1-cln.swf.gz", 1);		//workload 1
				//"Simulation Files/OSC-Clust-2000-3.1-cln.swf.gz", 1); 	//workload 2
				//"Simulation Files/LLNL-Atlas-2006-2.1-cln.swf.gz", 1);  	//workload 3
				//"Simulation Files/Sandia-Ross-2001-1.1-cln.swf.gz", 1); 	//workload 4
				//"Simulation Files/SDSC-Par-1996-3.1-cln.swf.gz", 1);		//workload 5
				//"Simulation Files/HPC2N-2002-2.1-cln.swf.gz", 1);         //workload 6
				//"Simulation Files/LCG-2005-1.swf.gz", 1);
				//"Simulation Files/SDSC-BLUE-2000-4.1-cln.swf.gz", 1);
				//"Simulation Files/LPC-EGEE-2004-1.2-cln.swf.gz", 1);
				//"Simulation Files/CEA-Curie-2011-2.1-cln.swf.gz", 1);
				
				
				
		// generate cloudlets from workload file
		cloudletList = workloadFileReader.generateWorkload();
		return cloudletList;
	}


	/**
	 * Prints the Cloudlet objects and Results of Simulation.
	 * 
	 * @param list
	 *            list of Cloudlets and list of VMs
	 * @throws IOException
	 */
	//Added the VM list as a parameter also and changed the name
	private static void printResults(List<Cloudlet> list, List<Vm> vlist, String simName, String workload, String simType, int expNum, int numPriorityVMs) throws IOException {
		int size = list.size();
		
		//added arrays to hold total CPU times and Wait times for each VM
		int numVMs = vlist.size();
		double[] totVmCpuTimes;
		double[] totVmEstCpuTime;
		int[] totNumCloudletsPerVm;
		totVmCpuTimes = new double[numVMs];
		totVmEstCpuTime = new double[numVMs];
		totNumCloudletsPerVm = new int[numVMs];
		
		Cloudlet cloudlet;

		ps.println();
		ps.println("========== OUTPUT ==========");

		ps.format("%-25s%-25s%-25s%-25s%-25s%-25s%-25s%-25s%-25s%n", 
				"Cloudlet_ID",
				"STATUS",
				"DataCenter_ID",
				"VM_ID",
				"Time",
				"Start_Time",
				"Finish_Time",
				"Total_Cloudlet_Length", 
				"Cloudlet_Length");


		
		for (int i = 0; i < size; i++) {
			cloudlet = list.get(i);
			
			//for each VM, if the current cloudlet runs on it, add actual CPU time to get total for VM
			for (int j = 0; j < numVMs; j++) {
				if (j == cloudlet.getVmId()) {
					totVmCpuTimes[cloudlet.getVmId()] += cloudlet.getActualCPUTime();
					totVmEstCpuTime[cloudlet.getVmId()] += cloudlet.getCloudletTotalLength()/(2*6000);
					totNumCloudletsPerVm[cloudlet.getVmId()]+= 1;
					
				}
			}
			
			
			ps.format("%-25d",cloudlet.getCloudletId());

			if (cloudlet.getCloudletStatus() == Cloudlet.SUCCESS) {
				ps.format("%-25s","SUCCESS");

				ps.format("%-25d%-25d%-25f%-25f%-25f%-25d%-25d%-25d%n",
						cloudlet.getResourceId(),
						cloudlet.getVmId(),
						cloudlet.getActualCPUTime(),
						cloudlet.getExecStartTime(), 
						cloudlet.getFinishTime(),
						cloudlet.getCloudletTotalLength(),
						cloudlet.getCloudletLength(),
						cloudlet.getNumberOfPes()
						);
			}
		}
		
		//Variables for Metrics of VMs
		double totalAccumulatedDebt = 0;
		double timeDiff = 0;
		double extraDebt = 0;
		double adjVmDebt = 0;
		double vmDebt = 0;
		double totalCpuNonPriority = 0;
		double totalCpuPriority = 0; 
		double pMarkedTime = 0; 
		double pMarkedDebt = 0;
		double totalAccumulatedDebtNonPriority = 0;
		double totalAccumulatedDebtPriority = 0;
		double totalAdjustedAccumulatedDebt = 0;
		
		ps.format("%n%n%-25s%-25s%-25s%n",
				"VM_ID",
				"TotVmTime", 
				"Total Debt",
				"Adjusted Debt");
		
		for (int j = 0; j < numVMs; j++) {
			Vm VM = vlist.get(j);
			
			
			if (VM.getPriorityLevel() == 0) { //calculates debt of a regular VM
				vmDebt =  (totVmCpuTimes[j]/60) * (.09/60);
				timeDiff = (totVmCpuTimes[j] - totVmEstCpuTime[j])/3600;
				extraDebt = timeDiff * 0.09;
				adjVmDebt = vmDebt - extraDebt;
				totalAccumulatedDebt += vmDebt;
				totalAdjustedAccumulatedDebt = adjVmDebt;
				totalAccumulatedDebtNonPriority += vmDebt;
				totalCpuNonPriority += totVmCpuTimes[j];
				if (VM.getPriorityMarker() == 1) {
					pMarkedTime += totVmCpuTimes[j];
					pMarkedDebt += vmDebt;
				}
				
			}
			else { 	//calculates debt of a priority VM
				vmDebt =  (totVmCpuTimes[j]/60)*(.15/60);
				adjVmDebt = 0;
				totalAccumulatedDebt += (totVmCpuTimes[j]/60)*(.15/60);
				totalAccumulatedDebtPriority += (totVmCpuTimes[j]/60)*(.15/60);
				totalCpuPriority += totVmCpuTimes[j];
				pMarkedTime += totVmCpuTimes[j];
				pMarkedDebt += vmDebt;
			}
			
			//Print debt for current VM
			ps.format("%-25d%-25f%-25f%-25f%n",
					j,	 
					totVmCpuTimes[j],
					vmDebt,
					adjVmDebt);
				
		}
		
		//Print the debt and CPU time of the total datacenter
			ps.format("%n%n%-50s%-25s%-25s%-25s%-25s%-25s%-25s%-25s%-25s%-25s%-25s%n", 
						"Simulation", 
						"Total Debt",
						"Adjusted Debt",
						"P Debt",
						"NP Debt",
						"P-Marked Debt",
						"NP-Marked Debt",
						"Total Time",
						"P Time",
						"NP Time",
						"P-Marked Time",
						"NP-Marked Time");
			 ps.format("%-50s%-25f%-25f%-25f%-25f%-25f%-25f%-25f%-25f%-25f%-25f%-25f%n",
					 simName,
					 totalAccumulatedDebt,
					 totalAdjustedAccumulatedDebt,
					 totalAccumulatedDebtPriority,
					 totalAccumulatedDebtNonPriority,
					 pMarkedDebt,
					 (totalAccumulatedDebt - pMarkedDebt),
					 (totalCpuNonPriority + totalCpuPriority),
					 totalCpuPriority,
					 totalCpuNonPriority,
					 pMarkedTime,
					 (totalCpuNonPriority + totalCpuPriority - pMarkedTime));
			 
			 	PrintWriter out = null;
			    BufferedWriter bufWriter;

			    try{
			        bufWriter =
			            Files.newBufferedWriter(
			                Paths.get("Simulation Files/output/log.txt"),
			                Charset.forName("UTF8"),
			                StandardOpenOption.WRITE, 
			                StandardOpenOption.APPEND,
			                StandardOpenOption.CREATE);
			        out = new PrintWriter(bufWriter, true);
			    }catch(IOException e){
			        //Oh, no! Failed to create PrintWriter
			    }

			    //After successful creation of PrintWriter
			    out.println(expNum + "," + 
			    			workload + "," +
			    			numVMs + "," + 
			    			simType + "," + 
			    			numPriorityVMs + "," +
						 	totalAccumulatedDebt + "," +
						 	totalAccumulatedDebtPriority + "," +
						 	totalAccumulatedDebtNonPriority + "," +
						 	(totalCpuNonPriority + totalCpuPriority) + "," +
						 	totalCpuPriority + "," +
						 	totalCpuNonPriority);

			    //After done writing, remember to close!
			    out.close();
			
	}
	
	
}