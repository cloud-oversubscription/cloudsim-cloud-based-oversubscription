package BG_Sim;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.StringTokenizer;

import org.cloudbus.cloudsim.examples.power.planetlab.LrMc;

/**
 * BgMigrationMain
 * This class contains the main method that drives the Experiment 3 simulations
 *
 */
public class BgMigrationMain {

	public static void main(String[] args) throws IOException {
		boolean enableOutput = true;
		boolean outputToFile = true;
		String inputFolder = LrMc.class.getClassLoader().getResource("workload/planetlab").getPath();
		
		BufferedReader in = new BufferedReader(new FileReader(inputFolder + "/control2_tso_mmt_test.txt")); 
		
		while (in.ready()) {
			String input = in.readLine();
			StringTokenizer tokenizer = new StringTokenizer(input,"|");
			String workload = tokenizer.nextToken();
			String vmAllocationPolicy = tokenizer.nextToken();
			String vmSelectionPolicy = tokenizer.nextToken();
			int percent = Integer.parseInt(tokenizer.nextToken()); 
			boolean migration = Boolean.parseBoolean(tokenizer.nextToken());
		
			
			String outputFolder = "output";
			//String workload = "20110322"; 
			//String vmAllocationPolicy = "tso"; 
			//String vmSelectionPolicy = "pr"; 
			String parameter = "1.2"; // the safety parameter of the LR policy
			double percentPriority = percent/100.0;
			//boolean migration = true;
			
	
			new BgPowerRunner(
					enableOutput,
					outputToFile,
					inputFolder,
					outputFolder,
					workload,
					vmAllocationPolicy,
					vmSelectionPolicy,
					parameter, 
					percentPriority,
					migration);
		}
		
		in.close();
	}

}
