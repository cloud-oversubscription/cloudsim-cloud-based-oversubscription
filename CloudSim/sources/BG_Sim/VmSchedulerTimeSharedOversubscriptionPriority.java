package BG_Sim;

//import java.io.FileNotFoundException;
//import java.io.FileOutputStream;
//import java.io.PrintStream;
import java.util.ArrayList;
//import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.cloudbus.cloudsim.Pe;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.VmSchedulerTimeSharedOverSubscription;
import org.cloudbus.cloudsim.lists.PeList;
//import org.cloudbus.cloudsim.lists.VmList;

/**
 * VmSchedulerTimeSharedOversubscriptionPriority
 * This class extends the VmSchedulerTimeSharedOverSubscription class.
 *
 */


public class VmSchedulerTimeSharedOversubscriptionPriority  extends VmSchedulerTimeSharedOverSubscription{
	
	private Map<String, List<Double>> priorityMipsMapRequested;
	
	public VmSchedulerTimeSharedOversubscriptionPriority(List<? extends Pe> pelist) {
		super(pelist);
		setPriorityMipsMapRequested(new HashMap<String, List<Double>>());
	}
	
	protected void setPriorityMipsMapRequested(Map<String, List<Double>> mipsMapRequested) {
		this.priorityMipsMapRequested = mipsMapRequested;
	}
	
	protected Map<String, List<Double>> getPriorityMipsMapRequested() {
		return priorityMipsMapRequested;
	}
	
	@Override
	public boolean allocatePesForVm(Vm vm, List<Double> mipsShareRequested) {	
		
		if (vm.isInMigration()) {
			if (!getVmsMigratingIn().contains(vm.getUid()) && !getVmsMigratingOut().contains(vm.getUid())) {
				getVmsMigratingOut().add(vm.getUid());
			}
		} else {
			if (getVmsMigratingOut().contains(vm.getUid())) {
				getVmsMigratingOut().remove(vm.getUid());
			}
		}
		
		boolean result = allocatePesForVm(vm.getUid(), mipsShareRequested, vm.getPriorityLevel());
		if(result) {
			updatePeProvisioning();
		}
		return result;
	}
	
	
	/**
	 * Allocate pes for vm. The policy allows over-subscription. In other words, the policy still
	 * allows the allocation of VMs that require more CPU capacity that is available.
	 * Oversubscription results in performance degradation. Each virtual PE cannot be allocated more
	 * CPU capacity than MIPS of a single PE.
	 * 
	 * @param vmUid the vm uid
	 * @param mipsShareRequested the mips share requested
	 * @return true, if successful
	 */
	protected boolean allocatePesForVm(String vmUid, List<Double> mipsShareRequested, int priorityClass) {
		double totalRequestedMips = 0;
		// if the requested mips is bigger than the capacity of a single PE, we cap
		// the request to the PE's capacity
		List<Double> mipsShareRequestedCapped = new ArrayList<Double>();
		//Map<String, List<Double>> mipsMapRequestedPriority = new HashMap<String, List<Double>>();
		
		double peMips = getPeCapacity();
		for (Double mips : mipsShareRequested) {
			if (mips > peMips) {
				mipsShareRequestedCapped.add(peMips);
				totalRequestedMips += peMips;
			} else {
				mipsShareRequestedCapped.add(mips);
				totalRequestedMips += mips;
			}
				
		}
		
		if (priorityClass == 1){
			priorityMipsMapRequested.put(vmUid, mipsShareRequested);
		}
	
		
		getMipsMapRequested().put(vmUid, mipsShareRequested);
		
		
		setPesInUse(getPesInUse() + mipsShareRequested.size());
				
		
		if (getVmsMigratingIn().contains(vmUid)) {
			// the destination host only experience 10% of the migrating VM's MIPS
			totalRequestedMips *= 0.1;
		}
		
	
		if (getAvailableMips() >= totalRequestedMips) {
			List<Double> mipsShareAllocated = new ArrayList<Double>();
			
			for (Double mipsRequested : mipsShareRequestedCapped) {
				if (getVmsMigratingOut().contains(vmUid)) {
					// performance degradation due to migration = 10% MIPS
					mipsRequested *= 0.9;
				} else if (getVmsMigratingIn().contains(vmUid)) {
					// the destination host only experience 10% of the migrating VM's MIPS
					mipsRequested *= 0.1;
				}
				mipsShareAllocated.add(mipsRequested);
			}

			getMipsMap().put(vmUid, mipsShareAllocated);
			
			setAvailableMips(getAvailableMips() - totalRequestedMips);
			
		} else {
			redistributeMipsDueToOverSubscription();			
		}

		return true;
	}

	/**
	 * This method recalculates distribution of MIPs among VMs considering eventual shortage of MIPS
	 * compared to the amount requested by VMs.
	 */
	@Override
	protected void redistributeMipsDueToOverSubscription() {
		// First, we calculate the scaling factor - the MIPS allocation for all VMs will be scaled
		// proportionally
		
		double totalRequiredMipsByAllVms = 0;
		double totalRequiredMipsByPriorityVms = 0;

		Map<String, List<Double>> mipsMapCapped = new HashMap<String, List<Double>>();
		Map<String, List<Double>> priorityMipsMapCapped = new HashMap<String, List<Double>>();
		
		for (Entry<String, List<Double>> entry : getMipsMapRequested().entrySet()) {

			double requiredMipsByThisVm = 0.0;
			String vmId = entry.getKey();

			List<Double> mipsShareRequested = entry.getValue();
			
			List<Double> mipsShareRequestedCapped = new ArrayList<Double>();
			List<Double> priorityMipsShareRequestedCapped = new ArrayList<Double>();
			
			double peMips = getPeCapacity();
			
			for (Double mips : mipsShareRequested) {
				if (mips > peMips) {
					mipsShareRequestedCapped.add(peMips);
					requiredMipsByThisVm += peMips;
					if(priorityMipsMapRequested.containsKey(vmId)) {
						priorityMipsShareRequestedCapped.add(peMips);
					}
				} else {
					mipsShareRequestedCapped.add(mips);
					requiredMipsByThisVm += mips;
					if(priorityMipsMapRequested.containsKey(vmId)) {
						priorityMipsShareRequestedCapped.add(mips);
					}
				}
			}

			mipsMapCapped.put(vmId, mipsShareRequestedCapped);
			priorityMipsMapCapped.put(vmId, priorityMipsShareRequestedCapped);			
			
			if (getVmsMigratingIn().contains(entry.getKey())) {
				// the destination host only experience 10% of the migrating VM's MIPS
				requiredMipsByThisVm *= 0.1;
			}
			
			if (priorityMipsMapRequested.containsKey(vmId)) {
				totalRequiredMipsByPriorityVms += requiredMipsByThisVm;
			}

			totalRequiredMipsByAllVms += requiredMipsByThisVm;
			
		}
		
	
		double totalAvailableMips = PeList.getTotalMips(getPeList());
		double scalingFactor;
		 

		if (totalRequiredMipsByAllVms - totalRequiredMipsByPriorityVms <= 0) {
			scalingFactor = 0;
		}
		else {
			 scalingFactor = (totalAvailableMips - totalRequiredMipsByPriorityVms)/(totalRequiredMipsByAllVms - totalRequiredMipsByPriorityVms);
		}
		
		
		// Clear the old MIPS allocation
		getMipsMap().clear();

		// Update the actual MIPS allocated to the VMs
		for (Entry<String, List<Double>> entry : mipsMapCapped.entrySet()) {
			String vmUid = entry.getKey();
			List<Double> requestedMips = entry.getValue();

			List<Double> updatedMipsAllocation = new ArrayList<Double>();
			for (Double mips : requestedMips) {
				
				if (getVmsMigratingOut().contains(vmUid)) {
					// the original amount is scaled if non-priority
					if (!priorityMipsMapRequested.containsKey(vmUid)) {
						mips *= scalingFactor;
					}
					// performance degradation due to migration = 10% MIPS
					mips *= 0.9;
				} 
				else if (getVmsMigratingIn().contains(vmUid)) {
					// the destination host only experiences 10% of the migrating VM's MIPS
					mips *= 0.1;
					// the final 10% of the requested MIPS are scaled
					if (!priorityMipsMapRequested.containsKey(vmUid)) {
						mips *= scalingFactor;
					}
				}
				else if (!priorityMipsMapRequested.containsKey(vmUid)) {
					mips *= scalingFactor;
				}	
					
				updatedMipsAllocation.add(Math.floor(mips));
			}

			// add in the new map
			getMipsMap().put(vmUid, updatedMipsAllocation);
			

		}

		// As the host is oversubscribed, there no more available MIPS
		setAvailableMips(0);
	}
	
	
	
	@Override
	public void deallocatePesForVm(Vm vm) {
		getMipsMapRequested().remove(vm.getUid());
		setPesInUse(0);
		getMipsMap().clear();
		setAvailableMips(PeList.getTotalMips(getPeList()));

		for (Pe pe : getPeList()) {
			pe.getPeProvisioner().deallocateMipsForVm(vm);
		}
		
		int priority;
		for (Map.Entry<String, List<Double>> entry : getMipsMapRequested().entrySet()) {
			if(this.priorityMipsMapRequested.containsKey(entry.getKey())) {
				priority = 1;
			}
			else {
				priority = 0;
			}
				allocatePesForVm(entry.getKey(), entry.getValue(), priority);
		}

		updatePeProvisioning();
	}
	
}

