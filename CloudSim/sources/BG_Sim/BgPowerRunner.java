package BG_Sim;

import java.util.Calendar;
import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.core.CloudSim;
import org.cloudbus.cloudsim.examples.power.RunnerAbstract;


/**
 * BgPowerRunner
 * This class extends the RunnerAbstract class used in the following paper:
 *
 * Anton Beloglazov, and Rajkumar Buyya, "Optimal Online Deterministic Algorithms and Adaptive
 * Heuristics for Energy and Performance Efficient Dynamic Consolidation of Virtual Machines in
 * Cloud Data Centers", Concurrency and Computation: Practice and Experience (CCPE), Volume 24,
 * Issue 13, Pages: 1397-1420, John Wiley & Sons, Ltd, New York, USA, 2012
 *
 * 
 */
public class BgPowerRunner extends RunnerAbstract {
	

	/**
	 * Instantiates a new planet lab runner.
	 * 
	 * @param enableOutput the enable output
	 * @param outputToFile the output to file
	 * @param inputFolder the input folder
	 * @param outputFolder the output folder
	 * @param workload the workload
	 * @param vmAllocationPolicy the vm allocation policy
	 * @param vmSelectionPolicy the vm selection policy
	 * @param parameter the parameter
	 */
	public BgPowerRunner(
			boolean enableOutput,
			boolean outputToFile,
			String inputFolder,
			String outputFolder,
			String workload,
			String vmAllocationPolicy,
			String vmSelectionPolicy,
			String parameter,
			double percentPriority, 
			boolean migration) {
		super(
				enableOutput,
				outputToFile,
				inputFolder,
				outputFolder,
				workload,
				vmAllocationPolicy,
				vmSelectionPolicy,
				parameter,
				percentPriority,
				migration);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cloudbus.cloudsim.examples.power.RunnerAbstract#init(java.lang.String)
	 */
	@Override
	protected void init(String inputFolder, double percentPriority) {
		try {
			CloudSim.init(1, Calendar.getInstance(), true);
			broker = BgPowerHelper.createBroker();
			int brokerId = broker.getId();
			cloudletList = BgPowerHelper.createCloudletList(brokerId, inputFolder);
			int numPriority = (int) Math.floor(cloudletList.size() * percentPriority);
			vmList = BgPowerHelper.createVmList(brokerId, cloudletList.size(), numPriority);
			hostList = BgPowerHelper.createHostList(BgPowerConstants.NUMBER_OF_HOSTS);
		} catch (Exception e) {
			e.printStackTrace();
			Log.printLine("The simulation has been terminated due to an unexpected error");
			System.exit(0);
		}
	}

}